Sviluppato da Andrea Piani

- Questo software è libero e open source.
- Per supportare il mio lavoro puoi farmi una donazione su [https://www.buymeacoffee.com/andreapiani
]()  oppure su [https://coindrop.to/andreapiani]()
- Puoi visitare il mio blog [https://www.andreapiani.com]()

# Bluetooth deauthenticator [blacktooth] v0.1
Uno script per il deauth degli aparecchi bluetooth


### Simple usage
```bash
Bluetooth deauthenticator v0.1.0-beta
Attack types:
        1.) l2ping - Ping flood
        2.) rfcomm - Connect flood

 [i] Usage: blue_dos.sh <target_addr> <packet_size> <attack_type>
root@kali:/scripts/blue-deauth# bash blue_dos.sh FC:58:FA:XX:XX:XX 600 1
root@kali:/scripts/blue-deauth# bash blue_dos.sh FC:58:FA:XX:XX:XX 600 2
```

### Requirements:
```
l2ping (comes with bluez)
rfcomm (comes with bluez)
```

### Tested only in Kali but any Linux OS that uses bluez should work.
---
## Pull requests and issues are welcome!
